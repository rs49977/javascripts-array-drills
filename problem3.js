
function sortCarsModel(cars){
    if (cars){
        for (let i=1; i< cars.length; i++){
            for(let j =0; j< cars.length-i; j++){
                if (cars[i].car_model < cars[j].car_model){
                    const temp = cars[i]
                    cars[i] = cars[j]
                    cars[j] = temp;
                }
            }
        }
    }
    return cars;
}

module.exports = sortCarsModel;