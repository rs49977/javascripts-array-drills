
function bmwAndAudiCars(cars){
    let requireCars = [];
    if (cars){
        for (let i=0; i< cars.length; i++){
             if (cars[i].car_make === 'Audi' || cars[i].car_make === 'BMW'){
                 requireCars.push(cars[i]);
            }
        }
    }
    return JSON.stringify(requireCars);
}

module.exports = bmwAndAudiCars;