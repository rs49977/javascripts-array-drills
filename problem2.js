
function lastCarInfo(cars){
    let lastCar = [];
    if (cars){
        let last = cars.length -1;
        lastCar.push(`Last car is a ${cars[last].car_make} ${cars[last].car_model}.`);
    }
    return lastCar;
}

module.exports = lastCarInfo;