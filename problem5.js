
function findOldCars(cars){
    let oldCars = [];
    if (cars){
        for (let i=0; i< cars.length; i++){
             if (cars[i].car_year < 2000){
                 oldCars.push(cars[i]);
            }
        }
    }
    return oldCars;
}

module.exports = findOldCars;