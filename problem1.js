
function carDetail(cars,num){
    let data = []
    if (cars && num){
        for (let i=0; i< cars.length; i++){
            if (cars[i].id === num){
                data.push(`Car ${num} is a ${cars[i].car_year} ${cars[i].car_make} ${cars[i].car_model}.`);
                break;
            }
        }
    }
    return data;
}

module.exports = carDetail;